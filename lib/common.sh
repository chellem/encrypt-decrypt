#!/bin/sh

# delimiter for seperating block of characters
delimiterList="|~*#"

# characters for space replacement
spaceReplacementList='^;/>'

# value to ascii value to shift (add or remove)
shiftValue=10