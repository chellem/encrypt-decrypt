#!/bin/sh

. ./lib/common.sh
. ./lib/convert.sh

# shift the character value by adding value to its octal value
doShift(){
    retval=`charToOctal $1`
    retval=`expr $retval - $2`
    state=0
    if [ $retval -gt 137 -a $retval -lt 140 ]
    then
        state=1
    fi
    if [ $retval -gt 77 -a $retval -lt 100 ]
    then
        state=1
    fi
    if [ $retval -gt 37 -a $retval -lt 40 ]
    then
        state=1
    fi
    if [ $state -eq 1 ]
    then
        retval=`expr $retval + 3`
    fi
    octalToChar $retval
}

# unshift the character value by substracting value to its octal value
doReverseShift(){
    retval=`charToOctal $1`
    retval=`expr $retval + $2`
    octalToChar $retval
}

# encode a character
encode(){
    char=`doShift $1 $2`
    echo $char
}

# get character from a specify position
charAt(){
    echo "`expr substr "$1" "$2" 1`"
}

# get a delimiter character in random order
getDelimiter(){
    position=`rand 1 4`
    char=`expr substr $delimiterList $position 1`
    echo "$char"
}

# get a space character replacement in a random order
getSpaceReplacement(){
    spacePos=`rand 1 4`
    space=`expr substr $spaceReplacementList $spacePos 1`
    echo "$space"
}

# check if character is a delimiter character
isDelimiter(){
    status=0
    length=`strLength $delimiterList`
    while [ $length -ne 0 ]
    do
        char=`charAt $delimiterList $length`
        if [ "$1" = "$char" ]
        then
             status=1
             break
        fi
        length=`expr $length - 1`
    done
    echo $status
}