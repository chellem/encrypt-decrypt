#!/bin/sh

# read input from terminal
readInput(){
    read -r -p"$1: " value
    echo "$value"
}