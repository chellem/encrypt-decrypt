#!/bin/sh

. ./lib/int.sh
. ./lib/char.sh

# get string length
strLength(){
    echo `expr length "$1"`
}

# replace space to be able to encode the character properly
replaceSpace(){
    replacement=`getSpaceReplacement`
    echo $1 | tr ' ' $replacement
}

# revert the replaced space to its original state
revertSpace(){
    string=$1
    spaceListCount=`strLength $spaceReplacementList`
    while [ $spaceListCount -ne 0 ]
    do
        spacer=`expr substr $spaceReplacementList $spaceListCount 1`
        string=`echo $string | tr "$spacer" ' '`
        spaceListCount=`expr $spaceListCount - 1`
    done
    echo $string
}