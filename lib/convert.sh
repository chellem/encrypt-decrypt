#!/bin/sh

# convert character to its octal value
charToOctal(){
    printf '%o' "'$1'"
}

# convert octal value to its character equivalence
octalToChar(){
    printf "\\$1"
}