#!/bin/sh

# generate random number between 2 range of values
rand(){
    seq $1 $2 | shuf -n ${3:-1}
}