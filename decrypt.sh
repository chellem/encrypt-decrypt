#!/bin/sh

. ./lib/IO.sh
. ./lib/char.sh
. ./lib/string.sh
. ./lib/common.sh

input=`readInput "Enter your text"`
count=`strLength "$input"`
charLength=0
temp=0
output=""
while [ "$count" -gt 0 ]
do
    inputChar=`charAt "$input" $count`
    state=`isDelimiter "$inputChar"`
    if [ $state -eq 0 ]
    then
        if [ $temp -gt 0 ]
        then
            newChar=`doReverseShift "$inputChar" $shiftValue`
            output=$newChar$output
            temp=0
        fi
    else
        temp=`expr $temp + 1`
    fi
    count=`expr $count - 1`
done

echo -e "\nDecrypted text:"
revertSpace "$output" $spaceReplacement