#!/bin/sh

. ./lib/IO.sh
. ./lib/char.sh
. ./lib/string.sh
. ./lib/common.sh

input=`readInput "Enter your text"`
input=`replaceSpace "$input"`
length=`strLength "$input"`
count=0
temp=0
output=""
while [ "$count" -ne "$length" ]
do
    sequence=""
    count=`expr $count + 1`
    rand=`rand 2 3`
    temp=$rand
    # expand each character by a random value
    while [ "$temp" -ne 0 ]
    do
        char=`charAt "$input" $count`
        encodeChar=`encode $char $shiftValue`
        sequence="$encodeChar$sequence"
        temp=`expr $temp - 1`
    done
    delimiter=`getDelimiter`
    sequence="$sequence$delimiter"
    output="$output$sequence"
done

echo -e "\nEncrypted text:\n$output"
